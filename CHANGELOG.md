# Changelog

All notable changes are documented in this file using the [Keep a CHANGELOG](http://keepachangelog.com/) principles.

## [1.1.2-beta] - 2018-07-15

### Added

* First version finalized.
