import * as mustache from 'mustache';

export function render (template: string, data: object = {})
{
	return mustache.render(template, data);
}
