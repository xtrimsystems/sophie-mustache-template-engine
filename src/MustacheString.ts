import { TemplateEngine } from 'sophie-view';

import * as MustacheWrapper from './MustacheWrapper';

export class MustacheString implements TemplateEngine
{
	private readonly template: string;

	public constructor (template: string)
	{
		this.template = template;
	}

	public async render (el: HTMLElement, data?: object): Promise<HTMLElement>
	{
		return new Promise<HTMLElement>(((resolve, reject) => {
			try {
				el.innerHTML = MustacheWrapper.render(this.template, data);
				resolve(el);
			} catch (e) {
				reject(e);
			}
		}));
	}
}
