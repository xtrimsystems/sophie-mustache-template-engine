import { HtmlRequestContentType, Query, RequestContentType, XhrResponse } from 'sophie-http';
import { TemplateEngine } from 'sophie-view';

import * as MustacheWrapper from './MustacheWrapper';

export class MustacheHttp implements TemplateEngine
{
	private readonly templatePath: string;
	private readonly query: Query;
	private readonly requestContentType: HtmlRequestContentType;
	private xhrResponseCached: XhrResponse;

	public constructor (
		templatePath: string,
		requestContentType: RequestContentType = new HtmlRequestContentType(),
		query: Query = new Query(),
	) {
		this.templatePath = templatePath;
		this.query = query;
		this.requestContentType = requestContentType;
	}

	public async render (el: HTMLElement, data?: object): Promise<HTMLElement>
	{
		return new Promise<HTMLElement>((resolve, reject) => {
			this.load(this.templatePath)
				.then((response: XhrResponse) => {
					this.cacheResponse(response);
					el.innerHTML = MustacheWrapper.render(response.text, data);
					resolve(el);
				})
				.catch(reject);
		});
	}

	private async load (templateName: string): Promise<XhrResponse>
	{
		if (this.isTemplateLoaded()) {
			return Promise.resolve(this.xhrResponseCached);
		}

		this.query.setUrl(`/${templateName}`);

		return this.query.getData(this.requestContentType);
	}

	private cacheResponse (response: XhrResponse): void
	{
		this.xhrResponseCached = response;
	}

	private isTemplateLoaded (): boolean
	{
		return typeof this.xhrResponseCached !== 'undefined';
	}
}
