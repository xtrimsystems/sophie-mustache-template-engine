# Sophie Mustache Template Engine

Template engine for [sophie-view](https://www.npmjs.com/package/sophie-view).
Wraps [mustache](https://www.npmjs.com/package/mustache) providing the ability to render html from a string
or to download a template via XMLHttpRequest

**Part of the framework [Sophie](https://bitbucket.org/xtrimsystems/sophie)**

[![npm version](https://badge.fury.io/js/sophie-mustache-template-engine.svg)](https://badge.fury.io/js/sophie-mustache-template-engine)
[![npm dependencies](https://david-dm.org/xtrimsystems/sophie-mustache-template-engine.svg)](https://www.npmjs.com/package/sophie-mustache-template-engine?activeTab=dependencies)
[![npm downloads](https://img.shields.io/npm/dm/sophie-mustache-template-engine.svg)](https://www.npmjs.com/package/sophie-mustache-template-engine)

## INSTALLATION
```shell
yarn add sophie-mustache-template-engine
```

## USAGE

Use it together with [sophie-view](https://www.npmjs.com/package/sophie-view)
AbstractRenderableView. See how to combine them in the [readme](https://www.npmjs.com/package/sophie-view#abstractrenderableview)

## Changelog
[Changelog](https://bitbucket.org/xtrimsystems/sophie-mustache-template-engine/src/master/CHANGELOG.md)

## Contributing

[![contributions welcome](https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat)](https://github.com/dwyl/esta/issues)

## License
This software is licensed under the terms of the [MIT license](https://opensource.org/licenses/MIT). See [LICENSE](https://bitbucket.org/xtrimsystems/sophie-mustache-template-engine/src/master/LICENSE) for the full license.
