import { render } from '../src';

describe('Test MustacheWrapper', () => {
	describe('method "render"', () => {
		it('should return the expected rendered template', () => {
			const template = document.createElement('DIV');
			template.innerHTML = `<span>Hello {{ key }}</span>`;

			let renderedTemplate = render(template.innerHTML, { key: 'Test' });

			expect(renderedTemplate).toBe('<span>Hello Test</span>');

			renderedTemplate = render(template.innerHTML);

			expect(renderedTemplate).toBe('<span>Hello </span>');
		});
	});
});
